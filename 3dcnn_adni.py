from __future__ import division
import os
import random
import time
import math


import pandas as pd
import numpy as np
import nibabel as nb
from PIL import Image
import matplotlib.pyplot as plt
import tensorflow as tf
import scipy.ndimage.interpolation as intp
import cv2


class Cnn3DMRI(object):


    def weight_variable(self, shape):
        initial = tf.truncated_normal(shape, stddev=0.1)
        return tf.Variable(initial)

    def bias_variable(self, shape):
        initial = tf.constant(0.1, shape=shape)
        return tf.Variable(initial)

    def conv3d(self, x, W):
        return tf.nn.conv3d(x, W, strides=[1, 1, 1, 1, 1], padding='SAME')

    def maxpool3d(self, x):
        #                        size of window         movement of window
        return tf.nn.max_pool3d(x, ksize=[1, 2, 2, 2, 1], strides=[1, 2, 2, 2, 1], padding='SAME')

    def dense_to_one_hot(self, labels_dense, num_classes):
        """Convert class labels from scalars to one-hot vectors."""
        num_labels = labels_dense.shape[0]
        index_offset = np.arange(num_labels) * num_classes
        labels_one_hot = np.zeros((num_labels, num_classes))
        labels_one_hot.flat[index_offset + labels_dense.ravel()] = 1
        return labels_one_hot

    def load_data(self, filenameX):
        img = nb.load(filenameX)  # Load the nii file
        data = img.get_data()  # a Numpy matrix
        return data

    def print_slice1(self, data):
        # save an image form the data set to be seen
        im = Image.fromarray(np.array(self.normalize(data), dtype=np.uint8))
        im = im.convert('L')
        im.show()
        return im

    def normalize(self, arr):
        "Normalaze the pixerl intenstity of an image "
        arr = np.array(arr, dtype=np.float32)
        minval = arr.min()
        maxval = arr.max()
        if minval != maxval:
            arr -= minval
            arr *= (1.0/(maxval-minval))
        return arr

    def softmax(self, x):
        res = []
        for batch_size in x:
            z_exp = [math.exp(i) for i in batch_size]
            sum_z_exp = sum(z_exp)
            res.append([round(i / sum_z_exp, 2) for i in z_exp])
        return res

    def shufle_data(self, data, label):
        a = data
        b = label
        combined = list(zip(data, label))
        random.shuffle(combined)
        a[:], b[:] = zip(*combined)
        shf_data = a
        shf_label = b

        return shf_data, shf_label

    def load_dataset(self, data_dir_all, labels_dir_all,  train_size=0.75, validation_size=0.1):
        """

        Args:
            labels_dir_all(pandas_cvs): the pandas
            data_dir_all(the location of the images to be load ):
            train_size(float): the part of the data dedicated to train (from 0 to 1)
            validation_size(float): the part of the data dedicated to validation (from 0 to 1)
        Returns:
            data_trainning(list) : the list of trainning set images
            label_trainning (list): the list of trainning set labels
            data_testing (list): the list of testing set images
            label_testing(list): the list of testing set labels

        """
        data_img = []
        data_img_oasi = []
        data_img_AD = []
        data_img_MCI = []
        data_img_NL = []
        data_img_AD_oasis = []
        data_img_MCI_oasis = []
        data_img_NL_oasis = []
        data_trainning = None
        label_trainning = None
        data_validation = None
        label_validation = None
        data_testing = None
        label_testing = None

        if np.shape(data_dir_all)[0] == 2:
            data_dir, data_dir_oasis = data_dir_all
            labels_dir, labels_dir_oasis = labels_dir_all
            patients_oasis = os.listdir(data_dir_oasis)
            patients_oasis.sort()
            labels_df_oasis = pd.read_csv(labels_dir_oasis, sep=',', index_col=1)
            for patient in patients_oasis:
                if patient[0] == '.':
                    pass
                else:
                    try:
                        label = labels_df_oasis.get_value(patient, 'Group')
                        CDR = labels_df_oasis.get_value(patient, 'CDR')
                    except Exception:
                        pass
                    else:
                        path_midd = data_dir_oasis+patient
                        for fld in os.listdir(path_midd):
                            path_midd_1 = data_dir_oasis+patient+'/'+fld
                            if fld[0] == '.' or fld != 'RAW':
                                pass
                            else:
                                for fld_2 in os.listdir(path_midd_1):
                                    if fld_2[0] == '.':
                                        pass
                                    else:
                                        path_midd_img = path_midd_1+'/'+fld_2
                                        if path_midd_img[-4:] == ".img":
                                            try:
                                                data_img_oasi.append(path_midd_img)
                                            except Exception:
                                                pass
                                            else:
                                                if label == 'Nondemented' or label == 0:
                                                    if CDR == 0.0:
                                                        label = 0
                                                        data_img_NL_oasis.append(path_midd_img)
                                                elif label == 'Converted' or label == 1:
                                                    if CDR == 0.5:
                                                        data_img_MCI_oasis.append(path_midd_img)
                                                        label = 1
                                                    elif CDR > 0.5:
                                                        data_img_AD_oasis.append(path_midd_img)
                                                        label = 2
                                                elif label == 'Demented' or label == 2:
                                                    if CDR == 0.5:
                                                        data_img_MCI_oasis.append(path_midd_img)
                                                        label = 1
                                                    elif CDR > 0.5:
                                                        data_img_AD_oasis.append(path_midd_img)
                                                        label = 2
        else:
            data_dir = data_dir_all[0]
            labels_dir = labels_dir_all[0]

        patients_adni = os.listdir(data_dir)
        patients_adni.sort()
        labels_df = pd.read_csv(labels_dir, sep=',', index_col=2)

        for patient in patients_adni:
            if patient[0] == '.':
                pass
            else:
                try:
                    label=labels_df.get_value(patient, 'Diag')
                except:
                    pass
                else:
                    path_midd=data_dir+patient
                    for fld in os.listdir(path_midd):
                        path_midd_1=data_dir+patient+'/'+fld
                        if fld[0]=='.':
                            pass
                        else:
                            for fld_2 in os.listdir(path_midd_1):
                                if fld_2[0] == '.':
                                    pass
                                else:
                                    path_midd_2=path_midd_1+'/'+fld_2
                                    for fld_3 in os.listdir(path_midd_2):
                                        if fld_3[0]=='.':
                                            pass
                                        else:
                                            path_midd_3=path_midd_2+'/'+fld_3
                                            for fld_4 in os.listdir(path_midd_3):
                                                if fld_4[0]=='.':
                                                    pass
                                                else:
                                                    path_midd_img=path_midd_3+'/'+fld_4
                                                    if path_midd_img[-4:] == ".nii":
                                                        try:
                                                            data_img.append(path_midd_img)
                                                        except:
                                                            pass
                                                        else:
                                                            if label=='NL' or label==0:
                                                                label=0
                                                                data_img_NL.append(path_midd_img)
                                                            elif label=='MCI' or label==1:
                                                                label=1
                                                                data_img_MCI.append(path_midd_img)
                                                            elif label=='AD' or label==2:
                                                                label=2
                                                                data_img_AD.append(path_midd_img)

        NL_sz = int(math.ceil(np.size(data_img_NL)*train_size))
        MCI_sz = int(math.ceil(np.size(data_img_MCI)*train_size))
        AD_sz = int(math.ceil(np.size(data_img_AD)*train_size))
        NL_sz_oasis = int(math.ceil(np.size(data_img_NL_oasis)*train_size))
        MCI_sz_oasis = int(math.ceil(np.size(data_img_MCI_oasis)*train_size))
        AD_sz_oasis = int(math.ceil(np.size(data_img_AD_oasis)*train_size))

        NL_sz_validation = int(math.ceil(np.size(data_img_NL)*validation_size))
        MCI_sz_validation = int(math.ceil(np.size(data_img_MCI)*validation_size))
        AD_sz_validation = int(math.ceil(np.size(data_img_AD)*validation_size))
        NL_sz_oasis_validation = int(math.ceil(np.size(data_img_NL_oasis)*validation_size))
        MCI_sz_oasis_validation = int(math.ceil(np.size(data_img_MCI_oasis)*validation_size))
        AD_sz_oasis_validation= int(math.ceil(np.size(data_img_AD_oasis)*validation_size))

        if self.train:
            print NL_sz, MCI_sz, AD_sz, NL_sz_oasis, MCI_sz_oasis, AD_sz_oasis
            if np.shape(data_dir_all)[0] == 2:

                data_trainning = data_img_NL[:NL_sz] + data_img_NL_oasis[:NL_sz_oasis] \
                                 + data_img_MCI[:MCI_sz] +data_img_MCI_oasis[:MCI_sz_oasis] \
                                 + data_img_AD[:AD_sz] + data_img_AD_oasis[:AD_sz_oasis]

                data_validation = data_img_NL[NL_sz:NL_sz_validation+NL_sz] + \
                                  data_img_NL_oasis[NL_sz_oasis:NL_sz_oasis+NL_sz_oasis_validation]+\
                                  data_img_MCI[MCI_sz:MCI_sz+MCI_sz_validation] + \
                                  data_img_MCI_oasis[MCI_sz_oasis:MCI_sz_oasis+MCI_sz_oasis_validation] + \
                                  data_img_AD[AD_sz:AD_sz+AD_sz_validation] + \
                                  data_img_AD_oasis[AD_sz_oasis:AD_sz_oasis+AD_sz_oasis_validation]


                label_trainning = np.concatenate(
                    ((np.ones([NL_sz+NL_sz_oasis]) * 0), (np.ones([MCI_sz+MCI_sz_oasis])),
                     (np.ones([AD_sz+AD_sz_oasis]) * 2))
                )

                label_validation = np.concatenate(
                    ((np.ones([NL_sz_validation + NL_sz_oasis_validation]) * 0),
                     (np.ones([MCI_sz_validation + MCI_sz_oasis_validation])),
                     (np.ones([AD_sz_validation + AD_sz_oasis_validation]) * 2)))

            else:

                #data_trainning = data_img_NL[:NL_sz] + data_img_MCI[:MCI_sz] + data_img_AD[:AD_sz]
                data_trainning = data_img_NL[:NL_sz] + data_img_AD[:AD_sz]

                label_trainning = np.concatenate(
                    ((np.ones([NL_sz]) * 0), (np.ones([AD_sz]) * 2))
                )

                data_validation = data_img_NL[NL_sz:NL_sz_validation+NL_sz] + \
                                  data_img_AD[AD_sz:AD_sz+AD_sz_validation]

                label_validation = np.concatenate(
                    ((np.ones([NL_sz_oasis_validation]) * 0),
                     (np.ones([AD_sz_oasis_validation]) * 2)))

            data_trainning, label_trainning = self.shufle_data(data_trainning,label_trainning)


            data_validation, label_validation = self.shufle_data(data_validation,label_validation)

        else:

            print int(math.ceil(np.size(data_img_NL)*(1-train_size-validation_size)))
            print int(math.ceil(np.size(data_img_MCI)*(1-train_size-validation_size)))
            print int(math.ceil(np.size(data_img_AD)*(1-train_size-validation_size)))
            print int(math.ceil(np.size(data_img_NL_oasis)*(1-train_size-validation_size)))
            print int(math.ceil(np.size(data_img_MCI_oasis)*(1-train_size-validation_size)))
            print int(math.ceil(np.size(data_img_AD_oasis)*(1-train_size-validation_size)))

            if np.shape(data_dir_all)[0] == 2:

                data_testing = data_img_NL[NL_sz+NL_sz_validation:] + \
                               data_img_NL_oasis[NL_sz_oasis+NL_sz_oasis_validation:] + \
                               data_img_MCI[MCI_sz+MCI_sz_validation:] + \
                               data_img_MCI_oasis[MCI_sz_oasis+MCI_sz_oasis_validation:] + \
                               data_img_AD[AD_sz+AD_sz_validation:] + \
                               data_img_AD_oasis[AD_sz_oasis+AD_sz_oasis_validation:]


                label_testing = np.concatenate(
                    (
                    (np.ones(np.shape(data_img_NL[NL_sz+NL_sz_validation:])[0]
                             + np.shape(data_img_NL_oasis[NL_sz_oasis+NL_sz_oasis_validation:])[0]) * 0),
                    (np.ones(np.shape(data_img_MCI[MCI_sz+MCI_sz_validation:])[0]
                             + np.shape(data_img_MCI_oasis[MCI_sz_oasis+MCI_sz_oasis_validation:])[0])),
                    (np.ones(np.shape(data_img_AD[AD_sz+AD_sz_validation:])[0] +
                             np.shape(data_img_AD_oasis[AD_sz_oasis+AD_sz_oasis_validation:])[0]) * 2)
                    )
                )

            else:
                data_testing = data_img_NL[NL_sz+NL_sz_validation:] + \
                               data_img_MCI[MCI_sz+MCI_sz_validation:] + \
                               data_img_AD[AD_sz+AD_sz_validation:]

                label_testing = np.concatenate((
                    (np.ones(np.shape(data_img_NL[NL_sz+NL_sz_validation:])[0]) * 0),
                    np.ones(np.shape(data_img_MCI[MCI_sz+MCI_sz_validation:])[0]),
                    (np.ones(np.shape(data_img_AD[AD_sz+AD_sz_validation:])[0]) * 2))
                )

            data_testing, label_testing = self.shufle_data(data_testing,label_testing)

        return data_trainning, label_trainning, data_validation, label_validation, data_testing,\
               label_testing

    def proces_data(self, patient, img_sz=150, n_slices=25, visual=False, firstsSlides=False):

        if not firstsSlides:
            if np.shape(patient)[2] > 120:
                patient = patient[:, :, np.int(.15 * np.shape(patient)[2]):-np.int(
                    .15 * np.shape(patient)[2])]  # get ride of tir
            else:
                patient = patient[:, :, np.int(.1 * np.shape(patient)[2]):-np.int(
                    .1 * np.shape(patient)[2])]  # get ride of tir

        new_img = np.zeros([np.shape(patient)[2], img_sz, img_sz])
        for slices in range(np.shape(patient)[2]):
            slice_norm = patient[:, :, slices]
            new_slice = np.array((cv2.resize(slice_norm, (img_sz, img_sz))), dtype=np.float32)
            new_img[slices, :, :] = new_slice
        patient = new_img

        # resize the num of slices of each patient

        zoom_factor = n_slices / np.shape(patient)[0]
        patient = intp.zoom(patient, [zoom_factor, 1, 1], order=0)
        # order defines the interpolation meth 0:nearest 1:bilinear, 3:cubic  ; The order has to
        #  be in the range 0-5

        # print all the new slices of a patient
        if visual:
            fig = plt.figure(figsize=(50, 50))
            for slices in range(np.shape(patient)[0])[10:20]:
                slc = patient[slices, :, :]
                y = fig.add_subplot(5, 5, slices + 1)
                y.imshow(slc, cmap='gray')
            plt.show()

        return patient

    def create_mean_image(self, img_sz, n_slices, data_all):
        mean_image = np.zeros([n_slices, img_sz, img_sz], dtype=np.float32)
        for image in data_all:
            try:
                image_load = self.load_data(image)
            except:
                np.add(mean_image, image_inter)
                continue
            image_inter = self.normalize(image_load)
            image_inter = self.proces_data(image_inter, img_sz=img_sz, n_slices=n_slices
                                           , visual=False, firstsSlides=False)
            mean_image = np.add(mean_image, image_inter)
        mean_image = np.divide(mean_image, np.shape(data_all))
        return mean_image

    def get_image(self, FilesList, label_2, last_batch, batch_size, img_sz, n_slices ):
        cnt = 0
        label = np.zeros(batch_size)
        BatchSet = np.zeros(shape=(batch_size,n_slices,img_sz,img_sz),dtype=np.float32)
        for x in range(last_batch,last_batch+batch_size):
            try:
                image=self.load_data(FilesList[x])
            except Exception:
                image = self.load_data(FilesList[x - 1])
                image = self.normalize(image)
                image = self.proces_data(image, img_sz=img_sz, n_slices=n_slices
                                         , visual=False, firstsSlides=False)
                image = image - self.mean_img
                BatchSet[cnt, :, :, :] = image
                label[cnt] = label_2[last_batch+cnt-1]
                cnt = cnt+1
                continue
            image = self.normalize(image)  # normalize all the pixels greyvalues between 0 and 255-
            image = self.proces_data(image, img_sz=img_sz, n_slices=n_slices
                                     , visual=False, firstsSlides=False)
            image = image - self.mean_img
            BatchSet[cnt, :, :, :] = image
            label[cnt] = label_2[last_batch+cnt]
            cnt = cnt+1
        new_batch = last_batch+batch_size
        return np.array([BatchSet, label, new_batch])

    def load_image(self, data_image, label_raw, img_sz, n_slices):
        label_out = np.zeros(len(label_raw))
        batch_set = np.zeros(shape=(len(data_image), n_slices, img_sz, img_sz), dtype=np.float32)
        for index, image in enumerate(data_image):
            try:
                image = self.load_data(image)
            except Exception, e:
                print e
                image = self.load_data(data_image[index-1])
                image = self.normalize(image)
                image = self.proces_data(image, img_sz=img_sz, n_slices=n_slices
                                         , visual=False, firstsSlides=False)
                image = image - self.mean_img
                batch_set[index, :, :, :] = image
                label_out[index] = label_raw[index-1]
                continue
            image = self.normalize(image)
            image = self.proces_data(image, img_sz=img_sz, n_slices=n_slices
                                     , visual=False, firstsSlides=False)
            image = image - self.mean_img
            batch_set[index, :, :, :] = image
            label_out[index] = label_raw[index]
        return batch_set, label_out

    def wrapper_image(self, full_image_set, full_label_set, last_batch=0, batch_size=5):
        batch_img = full_image_set[last_batch:batch_size+last_batch, :, :, :]
        batch_label = full_label_set[last_batch:batch_size+last_batch]
        return batch_img, batch_label, batch_size+last_batch


    def convolutional_neural_network(self, x, img_sz, n_slices):
        weights = {
            'W_conv1': self.weight_variable([6, 8, 8, 1, 32]),
            'W_conv2': self.weight_variable([2, 5, 5, 32, 48]),
            'W_conv3': self.weight_variable([2, 3, 3, 48, 64]),
            'W_conv4': self.weight_variable([2, 2, 2, 64, 80]),
            'W_fc': self.weight_variable([int(
                math.ceil(n_slices / 16) * (math.ceil(img_sz / 16) * math.ceil(img_sz / 16) *
                                            80)),
                                          2048]),
            'W_fc2': self.weight_variable([2048, 1024]),
            'out': self.weight_variable([1024, 2])
        }

        biases = {
            'b_conv1': self.bias_variable([32]),
            'b_conv2': self.bias_variable([48]),
            'b_conv3': self.bias_variable([64]),
            'b_conv4': self.bias_variable([80]),
            'b_fc': self.bias_variable([2048]),
            'b_fc2': self.bias_variable([1024]),
            'out': self.bias_variable([2])
        }

        self.x_im = tf.reshape(x, shape=[-1, n_slices, img_sz, img_sz, 1])

        conv1 = tf.tanh(self.conv3d(self.x_im, weights['W_conv1']) + biases['b_conv1'])
        conv1 =self.maxpool3d(conv1)

        conv2 = tf.tanh(self.conv3d(conv1, weights['W_conv2']) + biases['b_conv2'])
        conv2 = self.maxpool3d(conv2)

        conv3 = tf.tanh(self.conv3d(conv2, weights['W_conv3']) + biases['b_conv3'])
        conv3 = self.maxpool3d(conv3)
        conv4 = tf.tanh(self.conv3d(conv3, weights['W_conv4']) + biases['b_conv4'])

        fc = tf.reshape(conv4, [-1,int(math.ceil(n_slices/16)*math.ceil(img_sz/16)*math.ceil(
            img_sz/16))*80])
        fc = tf.tanh(tf.matmul(fc, weights['W_fc'])+biases['b_fc'])
        fc = tf.tanh(tf.matmul(fc, weights['W_fc2'])+biases['b_fc2'])
        fc = tf.nn.dropout(fc, self.keep_prob)

        output = tf.matmul(fc, weights['out'])+biases['out']
        return output

    def test_validation_set(self, data_validation, label_validation, valid_batch_size=60):

        batch_img, batch_label, last_batch = self.wrapper_image(
            data_validation, label_validation, self.last_valid_batch, valid_batch_size
        )


        if last_batch+valid_batch_size < len(label_validation):
            self.last_valid_batch = last_batch
        else:
            self.last_valid_batch = 0

        batch_label = self.dense_to_one_hot(
            np.array(batch_label, dtype=np.int),2
        ).astype(np.float32)

        pred = self.prediction.eval(
            feed_dict={self.x: batch_img, self.y_: batch_label, self.keep_prob: 1.0}
        )

        pred = self.softmax(pred)
        print "Prediction: "+str(pred)
        print "Label: "+str(batch_label)

        validation_accuracy = self.accuracy.eval(
            feed_dict={self.x: batch_img, self.y_: batch_label, self.keep_prob: 1.0})
        print "Validation accuracy: "+str(validation_accuracy)
        return validation_accuracy

    def train_neural_network(self, data_img, labels,  data_validation, label_validation,
                             batch_size, img_sz, n_slices, last_batch,
                             keep_rate, model_path):

        self.prediction = self.convolutional_neural_network(self.x, img_sz, n_slices)
        cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=self.y_,
                                                                      logits=self.prediction))
        optimizer = tf.train.AdamOptimizer(self.learning_rate).minimize(cost)
        correct_prediction = tf.equal(tf.argmax(self.prediction, 1), tf.argmax(self.y_, 1))
        self.accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

        hm_epochs = 1000
        saver = tf.train.Saver()
        epoch_loss = 0
        epoch_loss_counter = 0
        epoch_loss_mean = []
        n_epoch = 0
        learning_rate = 1e-4
        self.last_valid_batch = 0
        max_valid_accuracy = 0
        all_valid_accuracy = []
        model_path_train = 'model_train_3/my_model.ckpt'

        with tf.Session() as sess:
            sess.run(tf.global_variables_initializer())
            if model_path:
                pass
                 #saver.restore(sess, model_path_train)
            while n_epoch < hm_epochs:
                if len(data_img)>last_batch+batch_size:
                    with tf.device('/cpu:0'):
                        #batch_img, batch_label, last_batch = self.get_image(
                        #    data_img, labels, last_batch, batch_size, img_sz, n_slices
                        #)
                        batch_img, batch_label, last_batch = self.wrapper_image(data_img, labels, last_batch, batch_size)

                    print "Batch label images: "+str(batch_label)
                    batch_label = self.dense_to_one_hot(np.array(batch_label, dtype=np.int),
                                                        2).astype(np.float32)
                else:
                    with tf.device('/cpu:0'):
                        restbatch = last_batch + batch_size - len(data_img)
                        # batch_img = np.concatenate((
                        #    self.get_image(data_img, labels, last_batch, len(data_img) -
                        # last_batch, img_sz, n_slices)[0],
                        #    self.get_image(data_img, labels, 0, batch_size - (len(data_img) -
                        # last_batch), img_sz, n_slices)[0]
                        # ))
                        batch_img = np.concatenate((
                            self.wrapper_image(data_img, labels, last_batch, len(data_img) -
                                               last_batch)[0],
                            self.wrapper_image(data_img, labels, last_batch, len(data_img) -
                                               last_batch)[0]
                        ))


                        # batch_label = np.concatenate((
                        #     self.get_image(
                        #         data_img, labels, last_batch, len(data_img) - last_batch, img_sz, n_slices
                        #     )[1],
                        #
                        #     self.get_image(
                        #         data_img, labels, 0, batch_size - (len(data_img) - last_batch), img_sz, n_slices
                        #     )[1]
                        # ))
                        batch_label = np.concatenate((
                            self.wrapper_image(data_img, labels, last_batch, len(data_img) -
                                               last_batch)[1],
                            self.wrapper_image(data_img, labels, last_batch, len(data_img) -
                                               last_batch)[1]
                        ))

                    batch_label = self.dense_to_one_hot(np.array(batch_label, dtype=np.int),
                                                        2).astype(
                    np.float32)
                    last_batch = restbatch

                    ####### at the end of EACH EPOCH ###
                    epoch_loss_mean.append(epoch_loss)
                    print "epoch loss mean: "+str(epoch_loss_mean)
                    epoch_loss = 0
                    n_epoch += 1
                    print "n_epoch: "+str(n_epoch)
                    if not n_epoch % 3 and n_epoch > 100:
                        valid_accuracy = self.test_validation_set(data_validation,
                                                               label_validation, 60)
                        if valid_accuracy > max_valid_accuracy:
                            max_valid_accuracy = valid_accuracy
                            if model_path:
                                saver.save(sess, model_path)
                        all_valid_accuracy.append(valid_accuracy)
                        print all_valid_accuracy

                    elif not n_epoch % 5 and n_epoch < 100:
                        valid_accuracy = self.test_validation_set(data_validation,
                                       label_validation, 60)

                        if valid_accuracy > max_valid_accuracy:
                            max_valid_accuracy = valid_accuracy
                            if model_path:
                                saver.save(sess, model_path)
                        all_valid_accuracy.append(valid_accuracy)
                        print all_valid_accuracy

                    if self.last_valid_batch == 0:
                        self.shufle_data(data_validation, label_validation)

                    train_accuracy = self.accuracy.eval(
                        feed_dict={self.x: batch_img, self.y_: batch_label, self.keep_prob: 1.0})
                    print "trainning accuracy: " + str(train_accuracy)

                    self.shufle_data(data_img, labels)

                    if epoch_loss_counter > 100:
                        epoch_loss_counter = 0
                        learning_rate *= 0.5  # each 100 epochs we reduce the learning rate the
                        # half
                    else:
                        epoch_loss_counter += 1

                _, c = sess.run(
                    [optimizer, cost], feed_dict={
                        self.x: batch_img, self.y_: batch_label, self.keep_prob: keep_rate,
                        self.learning_rate: learning_rate
                    }
                )
                epoch_loss += c
                print 'epoch_loss: '+str(epoch_loss)

    def test_neural_network(self, data_img, labels, batch_size, img_sz, n_slices, last_batch,
                            model_path):

        prediction = self.convolutional_neural_network(self.x, img_sz, n_slices)
        correct_prediction = tf.equal(tf.argmax(prediction, 1), tf.argmax(self.y_, 1))
        accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
        try:
            print 'resotoring mean image'
            self.mean_img = np.load('mean_img_test.npy')
        except Exception:
            print 'mean image not found, creating and saving'
            self.mean_img = self.create_mean_image(img_sz, n_slices,  data_img)
            np.save('mean_img_test.npy', self.mean_img)
        saver = tf.train.Saver()
        with tf.Session() as sess:
            sess.run(tf.global_variables_initializer())
            saver.restore(sess, model_path)
            print("Model restored.")
            batch = self.get_image(data_img, labels, last_batch, batch_size, img_sz, n_slices)
            batch_img = batch[0]
            batch_label = batch[1]
            batch_label = self.dense_to_one_hot(np.array(batch_label, dtype=np.int), 2).astype(
                np.float32)

            train_accuracy = accuracy.eval(
                feed_dict={self.x: batch_img, self.y_: batch_label, self.keep_prob: 1.0}
            )

            print(self.softmax(prediction.eval({self.x: batch_img, self.keep_prob: 1.0})))
            print(" Accuracy %g" % (train_accuracy))

    def main(self, data_dir, labels_dir, img_sz, n_slices, batch_size=5, last_batch=0, train=False,
             model_path=None, keep_rate=0.5):
        """

        Args:
            data_dir(list): directories of the image to be tested
            labels_dir: (str): directory of the csv file where the image are labeled, the index
            colum is the number 2 and the labels header is 'Diag'.
            img_sz: the spatial image size the be transformed to. that is the sizes with which
            the image will be trainned. width and hight must be the same
            n_slices: the number of slices for the image to be trained
            last_batch: the batch at which you want to start the trainning
            train: boolean to set trainning: 0 or testing :1
            model_path: the path where the model is saved, if there is no previous model you can
            set a path here to start a new one.
            keep_rate: the keep_probability of firing a node by means of dropout

        Returns:

        """


        self.train = train
        data_path_trainning, label_trainning, data_path_validation, label_validation, \
        data_testing, label_testing = self.load_dataset(data_dir, labels_dir,)
        try:
            print 'resotoring mean image'
            self.mean_img = np.load('mean_img_validation.npy')

        except Exception:
            print 'mean image not found, creating and saving'
            self.mean_img = self.create_mean_image(img_sz, n_slices, data_path_trainning+data_path_validation)
            np.save('mean_img_validation.npy', self.mean_img)

        print 'Loading Trainning and validation images'

        data_trainning, label_trainning_final = self.load_image(data_path_trainning,
                                                                label_trainning, img_sz, n_slices
                                                                )

        data_validation, label_validation_final = self.load_image(
            data_path_validation, label_validation, img_sz, n_slices
        )


        self.x = tf.placeholder(tf.float32, shape=[None, n_slices, img_sz, img_sz]) #batch_size,
        # image_Size
        self.y_ = tf.placeholder(tf.float32, shape=[None, 2]) #batch_size, label_size
        self.learning_rate = tf.placeholder(tf.float32)
        self.keep_prob = tf.placeholder(tf.float32)
        if train:
            self.train_neural_network(data_trainning, label_trainning_final, data_validation,
                                      label_validation_final, batch_size, img_sz, n_slices,
                                      last_batch, keep_rate, model_path
                                      )
        else:
            if model_path:
                self.test_neural_network(data_testing, label_testing, batch_size, img_sz, n_slices,
                                         last_batch, model_path)
            else:
                print "For testing we need a model"
                raise IOError


if __name__ == '__main__':
    n_data_sets = 1
    model_path = 'modelo_path/my_model.ckpt'
    data_dir_adni = '/media/manel/INTENSO/python/StandarizedData/ADNI_1_Complete_1Yr_15T/ADNI_2/'
    labels_dir_adni = '/media/manel/Grande/tfm_130218/ADNI_complete.csv'
    data_dir_oasis = '/media/manel/Grande/Oasis_to_filter/oasis_tolad/'
    labels_dir_oasis = '/media/manel/Grande/tfm_130218/oasis_longitudinal_3.csv'
    if n_data_sets > 1:
        data_dir = [data_dir_adni, data_dir_oasis]
        labels_dir = [labels_dir_adni, labels_dir_oasis]
    else:
        data_dir = [data_dir_adni]
        labels_dir = [labels_dir_adni]
    batch_size = 10
    img_sz = 150
    n_slices = 40
    Cnn3d = Cnn3DMRI()
    Cnn3d.train = True

    Cnn3d.main(data_dir, labels_dir, img_sz, n_slices, batch_size, last_batch=0, train=True,
              model_path=model_path)